import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.Object;
import java.util.PriorityQueue;

class UnsortereHeap {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PriorityQueue<Integer> numbers = new PriorityQueue<>();

        for (String line = br.readLine(); line != null; line = br.readLine()) {
            int x = Integer.parseInt(line);
            numbers.add(x);
        }
	    
	divideintwoheaps(numbers);
    }

    static void divideintwoheaps(PriorityQueue<Integer> first) {
	PriorityQueue<Integer> second = new PriorityQueue<>();

	if (first.size() != 0) {
	int newsize= (first.size()-1)/2;
	int i=0;
	while ( i < newsize) {
	    int elem=first.poll();
	    //	    System.out.println("Temp "+elem);   
	    second.add(elem);
	    i++;
	}
	System.out.println(first.poll());

	divideintwoheaps(second);
	divideintwoheaps(first);
	}
    	return;
    }

}
