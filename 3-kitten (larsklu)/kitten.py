import sys

kitten = int(sys.stdin.readline().strip()) #set kitten as element on first line
graph = {} #initialize tree

#Build tree (dict) with parents as value, children as key.
for line in sys.stdin:
    lineData = line.split()
    value = int(lineData[0]) #set parent as value
    for key in lineData[1:]: #set children as key, (key can only occur once in input).
        graph[int(key)] = value

#Recursive function to find parent of the given node, until no more parents.
def findParent(node):
    sys.stdout.write((str(node)+' '))
    try:
        parent = graph[node]
        return findParent(parent)
    except KeyError:
        return

findParent(kitten)
