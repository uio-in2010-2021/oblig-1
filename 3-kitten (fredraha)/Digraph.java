import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.LinkedList;
import java.util.ArrayList;

public class Digraph {
    private final int V; // number of vertices
    private int E; // number of edges
    private ArrayList<LinkedList<Integer>> adj; // adjacency list (of lists)


    public Digraph(int V) { // 
        // int V = 100;
        this.V = V;
        this.E = 0;
        adj = new ArrayList<LinkedList<Integer>>();

        for (int v = 0; v < V; v++) {
            LinkedList<Integer> linkedList = new LinkedList<Integer>();
            adj.add(v, linkedList);
        }
    }

    public int V() {
        return V;
    }

    public int E() {
        return E;
    }

    public void addEdge(int v, int w) {
        adj.get(v).add(w);
        E++;
    }

    public Iterable<Integer> adj(int v) {
        return adj.get(v);
    }

    public Digraph reverse() {
        Digraph reversed = new Digraph(V);
        for (int v = 0; v < V; v++) {
            for (int w : adj(v)) {
                reversed.addEdge(w, v);
            }
        }
        return reversed;
    }

    public String get(int idx) {
        return adj.get(idx).toString();
    }
}