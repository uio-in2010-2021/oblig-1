import java.util.ArrayList;
import java.util.stream.Collectors;

public class DfsRootPath {
    private boolean[] marked; // keep track of visited nodes
    private ArrayList<Integer> path; // path so far
    private final int source; // place to search from

    public DfsRootPath(Digraph D, int source) {
        marked = new boolean[D.V()];
        path = new ArrayList<Integer>();
        this.source = source;
        path.add(source);
        dfs(D.reverse(), source);
    }

    private void dfs(Digraph D, int v) {
        marked[v] = true; // mark source as visited, then all other visits
        for (int w : D.adj(v)) { // for each adjacent neighbour, if not marked, set as marked and call dfs
            if (!marked[w]) {
                path.add(w);
                dfs(D, w);
            }
        }
    }

    public boolean hasPathTo(int v) {
        return marked[v];
    }

    public void printPath() {
        String joinedList = path.stream().map(String::valueOf).collect(Collectors.joining(" "));
        System.out.println(joinedList);
    }
}