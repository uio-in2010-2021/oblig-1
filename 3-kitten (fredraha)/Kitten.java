import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.LinkedList;


public class Kitten {
    public static void read_input(Scanner scanner) {
        BufferedReader bi = new BufferedReader(new InputStreamReader(System.in));
        String line;

        int v = 100; // max provided by Kattis
        Digraph digraph = new Digraph(v);
        int cat = -1;

        try {
            while ((line = bi.readLine()) != null) {
                String[] linesplit = line.split(" ");
                
                if (linesplit.length == 1) {
                    int value = Integer.valueOf(linesplit[0]);
                    if (value >= 0) {
                        cat = value;
                    } else {
                        break;
                    }
                } else {
                    int originVertex = Integer.valueOf(linesplit[0]);

                    for (int i = 1; i < linesplit.length; i++) {
                        int destinationVertex = Integer.valueOf(linesplit[i]);
                        digraph.addEdge(originVertex, destinationVertex);
                    }
                }
            }
        } catch (IOException e) {};

        scanner.close();

        DfsRootPath rootPath = new DfsRootPath(digraph, cat);
        rootPath.printPath();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        read_input(scanner);
    }
}